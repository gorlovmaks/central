<?php

namespace App\Repository;

use Psr\Log\LoggerInterface;
use App\Entity\LeasedObject;
use App\Model\Client\ObjectHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;


class LeasedObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry,ObjectHandler $objectHandler)
    {
        parent::__construct($registry, LeasedObject::class);
    }

    public function findByPriceObject($data_object)
    {

        $price_max = $data_object['price_max'];
        $price_min = $data_object['price_min'];
        $type_object = $data_object['type_object'];
        $name = $data_object['name'];

        try {
            $filtration =  $this->createQueryBuilder('l')
                ->select('l')
                ->where('l.price <= :price_max')
                ->andWhere('l.price >= :price_min')
                ->andWhere('l.name like :name')
                ->setParameter('price_min', $price_min ?? 0)
                ->setParameter('price_max', $price_max ?? 10000000000)
                ->setParameter('name',"%".$name."%");
                if($type_object === null){
                   return $filtration
                        ->getQuery()
                        ->getResult();
                }else{
                    return $filtration
                        ->andWhere('l.typeObject = :type_object')
                        ->setParameter('type_object',$type_object ?? 0)
                        ->getQuery()
                        ->getResult();
                }

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneById($id)
    {
        try {
            return $this->createQueryBuilder('l')
                ->select('l')
                ->where('l.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


}