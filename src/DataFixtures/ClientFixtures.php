<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Cottage;
use App\Entity\LeasedObject;
use App\Entity\Pension;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createNewClient([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
            'roles' => ['ROLE_TENANT']
        ]);

        $manager->persist($client);

        $client = $this->clientHandler->createNewClient([
            'email' => '321@321.ru',
            'passport' => 'passport & some',
            'password' => 'pass_4321',
            'roles' => ['ROLE_LANDLORD']
        ]);

        $manager->persist($client);
        $manager->flush();


        $pension = new Pension();

        $pension
            ->setName('Коллорадо')
            ->setPrice(1350)
            ->setAdress('Село Бостери')
            ->setContactPerson('Серьёзный человек')
            ->setNumberOfRooms(100)
            ->setTypeObject('Пансионат');
        $pension
            ->setDishes(true)
            ->setMassage(false)
            ->setMudVans(true);

        $manager->persist($pension);
        $manager->flush();

        $cottage = new Cottage();

        $cottage
            ->setName('Дельфин')
            ->setPrice(300)
            ->setAdress('Село Кара-ой')
            ->setContactPerson('Не серьезный человек')
            ->setNumberOfRooms(10)
            ->setTypeObject('Коттедж');
        $cottage
            ->setGoodAttitude(true)
            ->setSmokingRoom(false);

        $manager->persist($cottage);
        $manager->flush();

        $cottage = new Cottage();
        $cottage
            ->setName('Дельфин')
            ->setPrice(200)
            ->setAdress('Село Кара-ой')
            ->setContactPerson('Не серьезный человек')
            ->setNumberOfRooms(10)
            ->setTypeObject('Коттедж');
        $cottage
            ->setGoodAttitude(true)
            ->setSmokingRoom(false);

        $manager->persist($cottage);
        $manager->flush();

        $cottage = new Cottage();
        $cottage
            ->setName('Дельфин')
            ->setPrice(1000)
            ->setAdress('Село Кара-ой')
            ->setContactPerson('Не серьезный человек')
            ->setNumberOfRooms(10)
            ->setTypeObject('Коттедж');
        $cottage
            ->setGoodAttitude(true)
            ->setSmokingRoom(false);

        $manager->persist($cottage);
        $manager->flush();

        $cottage = new Cottage();
        $cottage
            ->setName('Дельфин')
            ->setPrice(600)
            ->setAdress('Село Кара-ой')
            ->setContactPerson('Не серьезный человек')
            ->setNumberOfRooms(10)
            ->setTypeObject('Коттедж');
        $cottage
            ->setGoodAttitude(true)
            ->setSmokingRoom(false);

        $manager->persist($cottage);
        $manager->flush();

        $pension = new Pension();

        $pension
            ->setName('Коллорадо')
            ->setPrice(1350)
            ->setAdress('Село Бостери')
            ->setContactPerson('Серьёзный человек')
            ->setNumberOfRooms(100)
            ->setTypeObject('Пансионат');
        $pension
            ->setDishes(true)
            ->setMassage(false)
            ->setMudVans(true);

        $manager->persist($pension);
        $manager->flush();

        $pension = new Pension();

        $pension
            ->setName('Коллорадо')
            ->setPrice(2000)
            ->setAdress('Село Бостери')



            ->setContactPerson('Серьёзный человек')
            ->setNumberOfRooms(500)
            ->setTypeObject('Пансионат');
        $pension
            ->setDishes(true)
            ->setMassage(false)
            ->setMudVans(true);

        $manager->persist($pension);
        $manager->flush();

        $pension = new Pension();

        $pension
            ->setName('Коллорадо')
            ->setPrice(1350)
            ->setAdress('Село Бостери')
            ->setContactPerson('Серьёзный человек')
            ->setNumberOfRooms(100)
            ->setTypeObject('Пансионат');
        $pension
            ->setDishes(true)
            ->setMassage(false)
            ->setMudVans(true);

        $manager->persist($pension);
        $manager->flush();
    }
}
