<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 06.07.18
 * Time: 13:41
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="date",nullable=true)
     */
    private $date_from;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $date_to;

    /**
     *
     * @ORM\Column(type="date",nullable=true)
     */
    private $currentDate;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="reservation")
     *
     */
    private $tenant;

    /**
     *
     * @var LeasedObject
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\LeasedObject", inversedBy="reservation")
     */
    private $leasedObject;


    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $room;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getLeasedObject()
    {
        return $this->leasedObject;
    }

    /**
     * @param  $leasedObject
     * @return Reservation
     */
    public function setLeasedObject($leasedObject)
    {
        $this->leasedObject = $leasedObject;
        return $this;
    }

    /**
     * @param string $room
     * @return Reservation
     */
    public function setRoom($room)
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param $tenant
     * @return Reservation
     */
    public function setTenant($tenant)
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return string
     */
    public function getTenant()
    {
        return $this->tenant;
    }

    /**
     * @param  $date_from
     * @return Reservation
     */
    public function setDateFrom( $date_from)
    {
        $this->date_from = $date_from;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateFrom()
    {
        return $this->date_from;
    }

    /**
     * @param  $date_to
     * @return Reservation
     */
    public function setDateTo( $date_to)
    {
        $this->date_to = $date_to;
        return $this;
    }

    /**
     * @return string
     */
    public function getDateTo()
    {
        return $this->date_to;
    }

    /**
     * @param  $currentDate
     * @return Reservation
     */
    public function setCurrentDate($currentDate)
    {
        $this->currentDate = $currentDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrentDate()
    {
        return $this->currentDate;
    }
}