<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 28.06.18
 * Time: 14:33
 */


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeasedObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"leasedobject" = "LeasedObject", "pension" = "Pension", "cottage" = "Cottage"})
 * @UniqueEntity("email")
 * @UniqueEntity("passport")
 * @Vich\Uploadable
 */

class LeasedObject
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     *
     *@var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $typeObject;


    /**
     * @var Reservation
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="leasedObject")
     */
    private $reservation;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $number_of_rooms;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $contact_person;

    /**
     *@var string
     *
     * @ORM\Column(type="decimal", length=64)
     */
    protected $price;

    /**
     *@var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $adress;

    /**
     *@var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $coordinates;


    /**
     * @return string
     */
    public function getTypeObject()
    {
        return $this->typeObject;
    }

    /**
     * @param string $typeObject
     * @return LeasedObject
     */
    public function setTypeObject($typeObject)
    {
        $this->typeObject = $typeObject;
        return $this;
    }

    /**
     * @param string $name
     * @return LeasedObject
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $number_of_rooms
     * @return LeasedObject
     */
    public function setNumberOfRooms($number_of_rooms)
    {
        $this->number_of_rooms = $number_of_rooms;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberOfRooms()
    {
        return $this->number_of_rooms;
    }


    /**
     * @param string $coordinates
     * @return LeasedObject
     */
    public function setCoordinates( $coordinates)
    {
        $this->coordinates = $coordinates;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }


    /**
     * @param string $contact_person
     * @return LeasedObject
     */
    public function setContactPerson( $contact_person)
    {
        $this->contact_person = $contact_person;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contact_person;
    }

    /**
     * @param string $price
     * @return LeasedObject
     */
    public function setPrice(string $price){
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $adress
     * @return LeasedObject
     */
    public function setAdress(string $adress)
    {
        $this->adress = $adress;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toArray() {
        return [
            'id' => $this->id,
            'type_object' => $this->typeObject,
            'name' => $this->name,
            'NumberOfRooms' => $this->number_of_rooms,
            'ContactPerson' => $this->contact_person,
            'price' => $this->price,
            'adress' => $this->adress,
            'phone' => $this->phone,
            'coordinates' => $this->coordinates
        ];
    }

    /**
     * @param string $phone
     * @return LeasedObject
     */
    public function setPhone( $phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $reservation
     * @return LeasedObject
     */
    public function setReservation( $reservation)
    {
        $this->reservation = $reservation;
        return $this;
    }

    /**
     * @return string
     */
    public function getReservation()
    {
        return $this->reservation;
    }

}