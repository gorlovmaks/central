<?php
/**
 * Created by PhpStorm.
 * User: maks
 * Date: 06.07.18
 * Time: 15:10
 */

namespace App\Controller;


use App\Entity\Reservation;
use App\Model\Client\ObjectHandler;
use App\Repository\ClientRepository;
use App\Repository\LeasedObjectRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class reservationController extends Controller
{

    /**
     * @Route("/reservation", name="app_add_reservation")
     * @param ClientRepository $clientRepository
     * @param ObjectHandler $objectHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @param LeasedObjectRepository $leasedObjectRepository
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function createReservation(
        ClientRepository $clientRepository,
        ObjectHandler $objectHandler,
        ObjectManager $manager,
        Request $request,
        LeasedObjectRepository $leasedObjectRepository,
        LoggerInterface $logger
    )
    {
        $client = $clientRepository->findOneByEmail($request->request->get('client_email'));


        $object = $leasedObjectRepository->findOneById($request->request->get('object_id'));



        $data['object_id'] = $object;
        $data['room'] = $request->request->get('room');
        $data['client'] = $client;

        $logger->info('dsadsadsadadsa',[$request->request->get('object_id')]);

        $reservation = $objectHandler->reservationObject($data);

        $manager->persist($reservation);

        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }
}