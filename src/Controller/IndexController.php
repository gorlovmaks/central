<?php

namespace App\Controller;

use App\Entity\LeasedObject;
use App\Model\Client\ClientHandler;
use App\Model\Client\ObjectHandler;
use App\Repository\ClientRepository;
use App\Repository\LeasedObjectRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/all/objects", name="app_index")
     * @param LeasedObjectRepository $leasedObjectRepository
     * @return JsonResponse
     */
    public function objectsAction(
        LeasedObjectRepository $leasedObjectRepository
    )
    {
        /**
         * @var  LeasedObject $object  */
        $objects = $leasedObjectRepository->findAll();
        $object_array = [];


        foreach ($objects as $object){
            $object_array [] = $object->__toArray();
        }


        return new JsonResponse($object_array);
    }

    /**
     * @Route("/filtration/objects", name="app_filtration")
     * @Method("GET")
     * @param LeasedObjectRepository $leasedObjectRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function objectsFiltrationAction(
        LeasedObjectRepository $leasedObjectRepository,
        Request $request
    )
    {

        $data_object['type_object'] = $request->query->get('type_object');
        $data_object['name'] = $request->query->get('name');
        $data_object['price_min'] = $request->query->get('price_min');
        $data_object['price_max'] = $request->query->get('price_max');

       /**
         * @var  LeasedObject $value  */

        $objectsByPrice = $leasedObjectRepository->findByPriceObject($data_object);
        $objects_by_price_array = [];
        ;
        foreach ($objectsByPrice as $value){
            $objects_by_price_array [] = $value->__toArray();
        }

        return new JsonResponse($objects_by_price_array);
    }

    /**
     * @Route("/client/password/encode", name="app_client_password_encode")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function passwordEncodeAction(
        ClientHandler $clientHandler,
        Request $request
    )
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePlainPassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }

    /**
     * @Route("/client/social/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientSocialAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check_client_credentials/{encodedPassword}/{email}", name="app_check_client_credentials")
     * @Method("HEAD")
     * @param string $encodedPassword
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientCredentialsAction(
        string $encodedPassword,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($encodedPassword, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {

        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');
        $data['roles'] = $request->request->get('roles');

        if (empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: ' . var_export($data, 1)], 406);
        }

        if ($clientRepository->findOneByPassportOrEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'], 406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/client/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByEmail($email);
        if ($client) {
            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client/email/{email}", name="app_patch_client_by_email")
     * @Method("PATCH")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function patchClientByEmailAction(
        string $email,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        Request $request,
        ObjectManager $manager
    )
    {
        $client = $clientRepository->findOneByEmail($email);
        if ($client) {
            $clientHandler->updateClient($client, [
                'email'    => $request->request->get('email', null),
                'passport' => $request->request->get('passport', null),
                'password' => $request->request->get('password', null),
                'network'  => $request->request->get('network', null)
            ]);
            $manager->flush();

            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/registerObject", name="app_create_object")
     * @Method("POST")
     * @param ObjectHandler $ObjectHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createObject(
        ObjectHandler $ObjectHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['smoking_room'] = $request->request->get('smoking_room');
        $data['price'] = $request->request->get('price');
        $data['type_object'] = $request->request->get('type_object');
        $data['adress'] = $request->request->get('adress');
        $data['NumberOfRooms'] = $request->request->get('NumberOfRooms');
        $data['name'] = $request->request->get('name');
        $data['ContactPerson'] = $request->request->get('ContactPerson');
        $data['dishes'] = $request->request->get('dishes');
        $data['coordinates'] = $request->request->get('coordinates');
        $data['phone'] = $request->request->get('phone');
        $data['mud_vuns'] = $request->request->get('mud_vuns');
        $data['massage'] = $request->request->get('massage');
        $data['GoodAttitude'] = $request->request->get('GoodAttitude');


        $object = $ObjectHandler->createNewAbstractObject(new LeasedObject(),$data);

        $manager->persist($object);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }
}
