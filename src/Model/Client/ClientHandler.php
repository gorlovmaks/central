<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = $this->encodePlainPassword($data['password']);
        $client->setPassword($password);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        $client->setRoles($data['roles']?? ['ROLE_USER']);

        return $client;
    }

    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return md5($password) . md5($password . '2');
    }

    public function updateClient(Client $client, array $data)
    {
        if($data['email'] !== null ) {
            $client->setEmail($data['email']);
        }
        if($data['passport'] !== null ) {
            $client->setPassport($data['passport']);
        }
        if($data['password'] !== null ) {
            $password = $this->encodePlainPassword($data['password']);
            $client->setPassword($password);
        }
        if($data['network'] !== null ) {
            foreach ($data['network'] as $network => $uid) {
                $client->setSocialId($network, $uid);
            }
        }
    }
}
