<?php


namespace App\Model\Client;


use App\Entity\Cottage;
use App\Entity\LeasedObject;
use App\Entity\Pension;
use App\Entity\Reservation;
use DateTime;


class ObjectHandler
{
    /**
     * @param array $data
     * @return LeasedObject|Cottage
     */
    public function createNewCottage(array $data) {
        $cottage = $this->createNewAbstractObject(new Cottage(), $data);
        /** @var Cottage $cottage */
        $cottage
            ->setSmokingRoom($data['smoking_room']??null)
            ->setGoodAttitude($data['GoodAttitude']??null);


        return $cottage;
    }

    /**
     * @param LeasedObject $object
     * @param array $data
     * @return LeasedObject
     */
    public function createNewAbstractObject(LeasedObject $object, array $data) {
        $object->setPrice($data['price']);
        $object->setTypeObject($data['type_object']);
        $object->setAdress($data['adress']);
        $object->setNumberOfRooms($data['NumberOfRooms']);
        $object->setCoordinates($data['coordinates']);
        $object->setPhone($data['phone']);
        $object->setName($data['name']);
        $object->setContactPerson($data['ContactPerson']);


        return $object;
    }

    /**
     * @param array $data
     * @return Reservation
     */
    public function reservationObject(array $data){

        $date = new DateTime();
        $reservation = new Reservation();
//        if($reservation->getDateFrom() === null){
//            $reservation->setCurrentDate($date);
//            $reservation->setDateFrom($date);
//            $reservation->setDateTo($date->modify('+7 day'));
//        }else{
//            $reservation->setCurrentDate($date);
//            $reservation->setDateFrom($reservation->getDateTo()->modify('+7 day'));
//        }
        $reservation->setLeasedObject($data['object_id']);

        $reservation->setRoom($data['room']);
        $reservation->setTenant($data['client']);

        return $reservation;
    }

    /**
     * @param array $data
     * @return LeasedObject|Pension
     */
    public function createNewPension(array $data) {
        $object = $this->createNewAbstractObject(new Pension(), $data);
        /** @var Pension $object */
        $object
            ->setMassage($data['massage']??null)
            ->setDishes($data['dishes']??null)
            ->setMudVans($data['mud_vuns']??null);

        return $object;
    }

}
